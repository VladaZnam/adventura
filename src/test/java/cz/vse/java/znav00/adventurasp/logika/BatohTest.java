package cz.vse.java.znav00.adventurasp.logika;


import javafx.scene.image.Image;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Trida BatohTest slouzi pro otestovani radneho ukldani veci do batohu
 *
 * @author    Vlada Znamenshchikova
 * @version  cerven 2020
 */
public class BatohTest {
    private Vec vec1;
    private Vec vec2;
    private Vec vec3;
    private Vec vec4;
    private Batoh batoh1;
    private Hra hra;
    HerniPlan herniPlan = new HerniPlan(hra);

    public BatohTest() {
    }

    @BeforeEach
    public void setUp() {

        this.vec1 = new Vec("drevo", "vetve, ktere mohou nakrmit Lucifera", true, new Image("/drevo.png"));
        this.vec2 = new Vec("drevo", "vetve, ktere mohou nakrmit Lucifera", true, new Image("/drevo.png"));
        this.vec3 = new Vec("kvetinka", "krasna kvetina, ktera by mohla byt jeste lepsi, ale nikdo ji nepokropi",false, new Image("/kvetinka/jpg"));
        this.vec4 = new Vec("klic", "klic, ktery otevira dvere do knihovny", true, new Image("/klic.png"));
        this.batoh1 = new Batoh(herniPlan);
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testVlozVecDoBatohu() {
      assertEquals(true, this.batoh1.vlozVeciDoBatohu(this.vec1));
        assertEquals(true, this.batoh1.jeVecVBatohu("drevo"));
        assertEquals(true, this.batoh1.vlozVeciDoBatohu(this.vec3));
      assertEquals(true, this.batoh1.jeVecVBatohu("kvetinka"));
       assertEquals(true, this.batoh1.vlozVeciDoBatohu(this.vec4));
       assertEquals(true, this.batoh1.jeVecVBatohu("klic"));
    }

    @Test
    public void testVyberVec() {
        assertEquals(true, this.batoh1.vlozVeciDoBatohu(this.vec1));
        assertEquals(this.vec1, this.batoh1.dejVecZBatohu("drevo"));
        assertEquals((Object)null, this.batoh1.dejVecZBatohu("star"));
    }
}
