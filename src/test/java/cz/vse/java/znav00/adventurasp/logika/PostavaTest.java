package cz.vse.java.znav00.adventurasp.logika;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * Trida PostavaTest comlexne otestuje tridu postava
 *
 * @author    Vlada Znamenshchikova
 * @version  cerven 2020
 */

public class PostavaTest {
    public PostavaTest(){

    }
    @BeforeEach
    public void setUp(){

    }
    @AfterEach
    public void tearDown(){
    }
    @Test
    //otestuju tady zda prirazena postava se opravdu v te mistnosti nachazi

    public void postavaVMistnosti(){
        Hra hra = new Hra();
        HerniPlan herniPlan = new HerniPlan(hra);
        Postava postava1 = new Postava( "Lucifer", "Kdo jsi? Co ode me chces? Jsem zly a hladovy demon a nereknu nic, dokud mi nedas jidlo!","Supr,uz nemam hlad a muzu ti pomoct. Ano, nas vladce je muj stary kamarad,\n  doufam, ze ho zachranis, drz klic, s nim muzes vstoupit do dalsi mistnosti. ", "Oblibena barva? Jojo, ja to vim! Oranzova. A vis proc?\n No protoze jsem oranzevy a vladce mi miluje :D ","");
        Postava postava2 = new Postava( "carodej", "Vitejte ve Svate knihovne, kde je ulozeno nejvetsi mnozstvi magie! \n Musis dokazat, ze mas dostatecne znalosti, abys osvobodil naseho vladce od kouzel! Hadej hadanku! \n *******... Kdyz cokoliv reknes, zabijes me... ******* ","Tak jsi sikovny! Muzu ti povolit jit k nasemu vladce! Tady je tvuh klic","Mluvis v oranzove knize? hmm, uvidime. Jo, jasne! Tady je zaklinadlo \n╔╔═══════════════════════════╗╗ \n" +
                "║║▐█▀▀▀▀▀▀▀▀▀▀║║▀▀▀▀▀▀▀▀▀▀▀█▌║║ \n" +
                "║║▐▌--------- ║║Zaklinadlo ▐▌║║ \n" +
                "║║▐▌--------- ║║           ▐▌║║ \n" +
                "║║▐▌--------- ║║*Trum-Trum*▐▌║║ \n" +
                "║║▐▌--------- ║║           ▐▌║║ \n" +
                "║║▐▌   -----  ║║           ▐▌║║ \n" +
                "║║▐▌          ║║           ▐▌║║ \n" +
                "║║▐█▄▄▄▄▄▄▄▄▄▄║║▄▄▄▄▄▄▄▄▄▄▄█▌║║ \n" +
                "╚╚═══════════════════════════╝╝ ","Tak jo, tady je oranzova khiha. Budte opatrni, tam se skryva velka magie");
        Postava postava3 = new Postava( "vladce", "proslov1","proslov2","ah joo... *neco mumle*...tak joo....Pockej kdo jsi? Co tady delas? Proc te sem ten carodej pustil??\n..Takze jsi splnil vsechny ukoly? Hura! Takze jsi ten, kdo me osvobodi! \n Zli demoni me okouzlili a ted jsem na vsechno zapomnel! Pomoz mi prosim! \n Ja vim ze odpoved je v knize me oblibene barvy, ale vubec nepamatuju ji..Ale to muze vedet muj kamarad","");

/*
        herniPlan.getAktualniProstor().setPostava(postava1);
        herniPlan.getAktualniProstor().setPostava(postava2);
        herniPlan.getAktualniProstor().setPostava(postava3);
        assertEquals("vladce", herniPlan.getAktualniProstor().getPostava().getJmeno());
        assertEquals("carodej", herniPlan.getAktualniProstor().getPostava().getJmeno());
        assertEquals("vladce", herniPlan.getAktualniProstor().getPostava().getJmeno());*/





    }
}