package cz.vse.java.znav00.adventurasp.logika;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/*******************************************************************************
 * Testovací třída HraTest slouží ke komplexnímu otestování
 * třídy Hra
 *
 * @author    Vlada Znamenshchikova
 * @version  cerven 2020
 */
public class HraTest {
    private Hra hra1;

    //== Datové atributy (statické i instancí)======================================

    //== Konstruktory a tovární metody =============================================
    //-- Testovací třída vystačí s prázdným implicitním konstruktorem ----------

    //== Příprava a úklid přípravku ================================================

    /***************************************************************************
     * Metoda se provede před spuštěním každé testovací metody. Používá se
     * k vytvoření tzv. přípravku (fixture), což jsou datové atributy (objekty),
     * s nimiž budou testovací metody pracovat.
     */
    @BeforeEach
    public void setUp() {
        hra1 = new Hra();
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každé testovací metody.
     */
    @AfterEach
    public void tearDown() {
    }

    //== Soukromé metody používané v testovacích metodách ==========================

    //== Vlastní testovací metody ==================================================

    /***************************************************************************
     * Testuje průběh hry, po zavolání každěho příkazu testuje, zda hra končí
     * a v jaké aktuální místnosti se hráč nachází.
     * Při dalším rozšiřování hry doporučujeme testovat i jaké věci nebo osoby
     * jsou v místnosti a jaké věci jsou v batohu hráče.
     * 
     */


    @Test
    public void testPrubehHry() {
        assertEquals("palouk", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("seber drevo");
        hra1.zpracujPrikaz("jdi pokoj_s_krbem");
        assertEquals(false, hra1.konecHry());
        assertEquals("pokoj_s_krbem", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("dej drevo");
        hra1.zpracujPrikaz("jdi knihovna");
        assertEquals(false, hra1.konecHry());
        assertEquals("knihovna", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("odpoved ticho");
        hra1.zpracujPrikaz("jdi pokoj_vladce");
        assertEquals(false, hra1.konecHry());
        assertEquals("pokoj_vladce", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("zaklinadlo Trum-Trum");
        assertEquals(true, hra1.konecHry());
    }
}
