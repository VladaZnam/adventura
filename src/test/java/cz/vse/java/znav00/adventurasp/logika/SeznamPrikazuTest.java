package cz.vse.java.znav00.adventurasp.logika;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Trida SeznamPrikazuTest slouzi pro otestovani tridy SeznamPrikazu
 *
 * @author    Vlada Znamenshchikova
 * @version  cerven 2020
 */


public class SeznamPrikazuTest {
    private Hra hra;
    private PrikazKonec prKonec;
    private PrikazJdi prJdi;

    @BeforeEach
    public void setUp(){
        hra = new Hra();
        prKonec = new PrikazKonec(hra);
        prJdi = new PrikazJdi(hra.getHerniPlan());

    }
    @AfterEach
    public void tearDown() {
    }
    @Test

    public void platnostPrikazu(){
        //otestuje platnost prikazu v mapu s prikazy
        SeznamPrikazu seznam = new SeznamPrikazu();
        seznam.vlozPrikaz(prKonec);
        seznam.vlozPrikaz(prJdi);
        assertEquals(true,seznam.jePlatnyPrikaz("konec"));
        assertEquals(true,seznam.jePlatnyPrikaz("jdi"));
        assertEquals(false,seznam.jePlatnyPrikaz("napoveda"));
        assertEquals(false,seznam.jePlatnyPrikaz("JDI"));

    }
    @Test
    public void vlozeniAVypis() {
        //otestuje funkci vratPrikaz - konkretne zda funkce vraci spravne podle daneho Stringu
        SeznamPrikazu seznam = new SeznamPrikazu();
        seznam.vlozPrikaz(prKonec);
        seznam.vlozPrikaz(prJdi);
        assertEquals(prJdi, seznam.vratPrikaz("jdi"));
        assertEquals(prKonec, seznam.vratPrikaz("konec"));
        assertEquals(null,seznam.vratPrikaz("rekni"));
    }

    @Test
    public void prikazy(){
        //otestuje zda se prikazy opravdu vlozily do seznamu prikazu a zzda je seznam obsahuje

        SeznamPrikazu seznam = new SeznamPrikazu();
        seznam.vlozPrikaz(prKonec);
        seznam.vlozPrikaz(prJdi);
        String prikazy = seznam.vratNazvyPrikazu();
        assertEquals(true,prikazy.contains("konec"));
        assertEquals(true,prikazy.contains("jdi"));
        assertEquals(false,prikazy.contains("napoveda"));
        assertEquals(false,prikazy.contains("JDI"));
    }

}