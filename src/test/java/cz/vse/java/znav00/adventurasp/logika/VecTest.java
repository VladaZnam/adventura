package cz.vse.java.znav00.adventurasp.logika;


import javafx.scene.image.Image;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class VecTest {
    public VecTest(){

    }
    @BeforeEach
    public void setUP(){
    }
    @AfterEach
    public void tearDown(){

    }
    @Test
    public void testPrenositelnost() {
        Vec vec1 = new Vec("drevo", "vetve, ktere mohou nakrmit Lucifera", true, new Image("/drevo.png"));
        Vec vec2 = new Vec("kvetinka", "krasna kvetina, ktera by mohla byt jeste lepsi, ale nikdo ji nepokropi",false, new Image("/kvetinka.png"));
        assertEquals(true, vec1.isPrenositelna());
        assertEquals(false, vec2.isPrenositelna());
    }




    }


