package cz.vse.java.znav00.adventurasp.logika;


import cz.vse.java.znav00.adventurasp.main.Observer;
import cz.vse.java.znav00.adventurasp.main.Subject;
import javafx.scene.image.Image;

import java.util.HashSet;
import java.util.Set;

/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 * @author    Vlada Znamenshchikova
 * @version  cerven 2020
 */

public class HerniPlan implements Subject {
    
    private Prostor aktualniProstor;
    private Batoh batoh;
    private Hra hra;


    private Set <Observer> seznamPozorovatelu;

    
     /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan(Hra hra) {
        this.zalozProstoryHry();
        this.batoh = new Batoh(this);
        this.hra = hra;

        seznamPozorovatelu = new HashSet<>();

    }

    public Hra getHra() {
        return hra;
    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory

        Prostor palouk = new Prostor("palouk", "palok vedle velkeho stromu");
        Prostor pokojSKrbem = new Prostor("pokoj_s_krbem","pokoj ve centre ktereho stoji krb s mluvicim ohnem");
        Prostor knihovna = new Prostor("knihovna","Velka staroveka knihovna, ve ktere jsou ulozeny vsechny nejstarsi kouzla tohoto sveta");
        Prostor pokojVladce = new Prostor("pokoj_vladce","pokoj, ve kterem bydli vladce");

        aktualniProstor = palouk; // hra začíná na palouku

        // přiřazují se průchody mezi prostory (sousedící prostory)
        palouk.setVychod(pokojSKrbem);
        pokojSKrbem.setVychod(knihovna);
        knihovna.setVychod(pokojVladce);
        pokojVladce.setVychod(knihovna);
        pokojVladce.setVychod(pokojSKrbem);
        pokojSKrbem.setVychod(palouk);
        knihovna.setVychod(pokojSKrbem);



        // vytvoreni veci do prostoru
        Vec drevo = new Vec("drevo", "vetve, ktere mohou nakrmit Lucifera", true, new Image("/drevo.png"));
        Vec kvetinka = new Vec("kvetinka", "krasna kvetina, ktera by mohla byt jeste lepsi, ale nikdo ji nepokropi",false, new Image("/kvetinka.png"));
        Vec klic = new Vec("klic", "klic, ktery otevira dvere do knihovny", true, new Image("/klic1.png"));
        Vec klic2 = new Vec( "klic2", "klic, ktery otevira dvere do pokoje vladce", true, new Image("/klic2.png"));
        Vec rouno = new Vec(" ", " ", true, new Image("/runa.png"));
        Vec kniha = new Vec ("kniha", "kniha pro vladce", true, new Image("/kniha.png"));

        palouk.pridejVec(drevo);
        palouk.pridejVec(kvetinka);

        // Vytvarime jednotlive postavy a pridame jim proslov

        Postava Lucifer = new Postava( "Lucifer", "Kdo jsi? Co ode me chces? Jsem zly a hladovy demon a nereknu nic,\n dokud mi nedas jidlo!","Supr,uz nemam hlad a muzu ti pomoct. Ano, nas vladce je muj stary kamarad,\n  doufam, ze ho zachranis, drz klic, s nim muzes vstoupit do dalsi mistnosti. ", "Oblibena barva? Jojo, ja to vim! Oranzova. A vis proc?\n No protoze jsem oranzevy a vladce mi miluje :D ","");
        Postava Carodej = new Postava( "carodej", "Vitejte ve Svate knihovne, kde je ulozeno nejvetsi mnozstvi magie! \n Musis dokazat, ze mas dostatecne znalosti, abys osvobodil naseho vladce od kouzel! Hadej hadanku! \n *******... Kdyz cokoliv reknes, zabijes me... ******* ","Tak jsi sikovny! Muzu ti povolit jit k nasemu vladce! Tady je tvuh klic","Mluvis v oranzove knize? hmm, uvidime. Jo, jasne! Tady je zaklinadlo" +
                "\n╔╔═══════════════════════════╗╗ \n" +
                "║║▐█▀▀▀▀▀▀▀▀▀▀║║▀▀▀▀▀▀▀▀▀▀▀█▌║║ \n" +
                "║║▐▌---------             ║║      Zaklinadlo     ▐▌║║ \n" +
                "║║▐▌---------             ║║                            ▐▌║║ \n" +
                "║║▐▌---------             ║║      *Trum-Trum* ▐▌║║ \n" +
                "║║▐▌---------             ║║                            ▐▌║║ \n" +
                "║║▐▌   -----                ║║                            ▐▌║║ \n" +
                "║║▐▌                          ║║                            ▐▌║║ \n" +
                "║║▐█▄▄▄▄▄▄▄▄▄▄║║▄▄▄▄▄▄▄▄▄▄▄█▌║║ \n" +
                "╚╚═══════════════════════════╝╝ "
                ,"Tak jo, tady je oranzova khiha. Budte opatrni, tam se skryva velka magie");
        Postava Vladce = new Postava( "vladce", "proslov1","proslov2","ah joo... *neco mumle*...tak joo....Pockej kdo jsi? Co tady delas? Proc te sem ten carodej pustil??\n..Takze jsi splnil vsechny ukoly? Hura! Takze jsi ten, kdo me osvobodi! \n Zli demoni me okouzlili a ted jsem na vsechno zapomnel! Pomoz mi prosim! \n Ja vim ze odpoved je v knize me oblibene barvy, ale vubec nepamatuju ji..Ale to muze vedet muj kamarad","");

        //tady pridavam do mistnosti postavy
        pokojSKrbem.setPostava(Lucifer);
        knihovna.setPostava(Carodej);
        pokojVladce.setPostava(Vladce);

        //pridavam postavam veci, ktere bodou chtit od hrace a veci ktere hraci zatim davaji
        Lucifer.setVecKterouSiPostavaVezme(new Vec("drevo","vetve, ktere mohou nakrmit Lucifera", true, new Image("/drevo.png")));
        Carodej.setVecKterouSiPostavaVezme2(new Vec("rouno", "barevne rouno, aby se nezapomnel na oblibenou barvu vladce", true, new Image("/runa.png")));

        //pridavame postavam veci ktere budou chtit od hrace a veci,ktere hraci zatim davaji
        Lucifer.setVecKterouPostavaDava(new Vec("klic", "klic, ktery otevira dvere do knihovny", true, new Image("/klic1.png")));
        //Lucifer.setVecKterouPostavaDava2(new Vec( " ", " ", true, obrazek));
        Carodej.setVecKterouPostavaDava2(new Vec("kniha","kniha pro vladce", true, new Image("/kniha.png")));
        Carodej.setVecKterouPostavaDava(new Vec("klic2","klic, ktery otevira dvere do pokoje vladce", true, new Image("/klic2.png")));



    }



    public Batoh getBatoh() {return this.batoh;}
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {

        aktualniProstor = prostor;
        notifyObservers();
    }

    @Override
    public void register(Observer observer) {
        seznamPozorovatelu.add(observer);

    }

    @Override
    public void unregister(Observer observer) {
        seznamPozorovatelu.remove(observer);

    }

    @Override
    public void notifyObservers() {
        for (Observer observer : seznamPozorovatelu) {
            observer.update();
        }

    }
}
