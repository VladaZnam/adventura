package cz.vse.java.znav00.adventurasp.logika;
/**
 * Trida PrikazZaklinadlo implementuje pro hru prikaz zaklinadlo
 * Trida ja soucasti jednoduche textove hry
 *
 * @author Vlada Znamenshchikova
 * @version cerven 2020
 */

public class PrikazZaklinadlo implements IPrikaz {
    HerniPlan herniPlan;
    private Hra hra;
    private static final String NAZEV = "zaklinadlo";


    /**
     * Konstruktor tridy
     * @param herniPlan
     */



    public PrikazZaklinadlo(HerniPlan herniPlan, Hra hra) {
        this.herniPlan = herniPlan;
        this.hra = hra;
    }


    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo, tak ....
            return "Co mam odpovedet? Musite zadat odpoved.";
        }
        else {
            if (herniPlan.getAktualniProstor().getNazev().equals("pokoj_vladce") ) {//jestlize v spravnem prostoru
                if (parametry.length == 1 && parametry[0].equals("Trum-Trum")) {//spravna odpoved
                   // this.herniPlan.getHra().setKonecHry(true);
                    {
                        System.out.println("Vyhral jsi"); }
                    hra.setKonecHry(true);

                } else {
               hra.setKonecHry(true);
                return "Prohral jsi";
            }} else {


            return "Nemuzete tady pouzivat zaklinadlo.";
        }} return "";

    }


    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     *  @return nazev prikazu
     */
    public String getNazev() {
        return NAZEV;
    }
}