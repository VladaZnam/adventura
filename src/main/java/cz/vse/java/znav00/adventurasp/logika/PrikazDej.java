package cz.vse.java.znav00.adventurasp.logika;
/**
 * Trida PrikazDej implementuje pro hru prikaz dej.
 * Trida ja soucasti jednoduche textove hry.
 *
 * @author Vlada Znamenshchikova
 * @version cerven 2020
 */

public class PrikazDej implements IPrikaz {
    private static final String NAZEV = "dej";
    private HerniPlan plan;

    public PrikazDej(HerniPlan plan) {
        this.plan = plan;
    }

    

    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Nevim co mam dat";
        }
        String nazevVeci = parametry[0];
        //zde to vezme vec z batohu, kterou to najde podle jmena
        if (plan.getBatoh().jeVecVBatohu(nazevVeci)) {
            if (plan.getAktualniProstor().getPostava() != null) {
                if (plan.getAktualniProstor().getPostava().getVecKterouSiPostavaVezme().getNazev().equals(nazevVeci)) {
                    //vec, kterou chci odevzdat postave se odstrani z batohu
                    plan.getBatoh().dejVecZBatohu(nazevVeci);
                    //vec, kterou mi postava da, se do batohu vlozi
                    Vec vecKterouDavaPostava = plan.getAktualniProstor().getPostava().getVecKterouPostavaDava();

                    if (vecKterouDavaPostava != null) {
                        plan.getBatoh().vlozVeciDoBatohu(vecKterouDavaPostava);
                                          }


                } else {
                    return "Toto neni vec,kterou postava chtela";
                }
            }
            else {
                return "mistnost je prazdna";
                }


        } else {
            return "Vec, kterou jsi zadal, neni v batohu ";
        }

        return plan.getAktualniProstor().getPostava().getTextKteryRekneKdyDostaneSvouVec();
    }
    /**
     * Metoda vraci nazev prikazu (slovo,ktere pouziva hrac pro jeho vyvolani)
     *
     * @return  nazev prikazu
     */

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
