package cz.vse.java.znav00.adventurasp.logika;

/** Trida Postava popisuje postvy, ktere se mohou nachazet v jednotlivych prostorech hry,
 * Kazda postava ma nejaky proslov, a take muze obdrzet veci a davat veci hraci
 * S postavou muzete mluvit
 *
 * @author Vlada Znamenshchikova
 * @version cerven 2020
 */


public class Postava {
    private String jmeno;
    private String proslov;
    private Vec vecKterouPostavaDava = null;
    private Vec vecKterouPostavaDava2 = null;
    private Vec vecKterouSiPostavaVezme = null;
    private Vec vecKterouSiPostavaVezme2 = null;
    private String textKteryRekneKdyzDostaneSvouVec;
    private String textKteryReknePoDruhe;
    private String textKteryRekneKdyzDostaneSvouVecPoDruhe;//TODO

    public Vec getVecKterouPostavaDava2() {
        return vecKterouPostavaDava2;
    }

    public void setVecKterouPostavaDava2(Vec vecKterouPostavaDava2) {
        this.vecKterouPostavaDava2 = vecKterouPostavaDava2;
    }

    public Vec getVecKterouSiPostavaVezme2() {
        return vecKterouSiPostavaVezme2;
    }

    public void setVecKterouSiPostavaVezme2(Vec vecKterouSiPostavaVezme2) {
        this.vecKterouSiPostavaVezme2 = vecKterouSiPostavaVezme2;
    }

    /** pridavam Postve vec, kterou bude chtit ot hrace
     *
     * @return vecKterouSiPostavaVezme vec, kterou potrebujeme
     */

    public Vec getVecKterouPostavaDava() {
        return vecKterouPostavaDava;
    }

    public void setVecKterouPostavaDava(Vec vecKterouPostavaDava) {
        this.vecKterouPostavaDava = vecKterouPostavaDava;
    }

    public Vec getVecKterouSiPostavaVezme() {
        return vecKterouSiPostavaVezme;
    }

    public void setVecKterouSiPostavaVezme(Vec vecKterouSiPostavaVezme) {
        this.vecKterouSiPostavaVezme = vecKterouSiPostavaVezme;
    }
    /** vraci text,ktery rekne,kdyz ziska nutnou vec
     *
     * @return druhy proslov
     */

    public String getTextKteryRekneKdyDostaneSvouVec() {
        return textKteryRekneKdyzDostaneSvouVec;
    }
    /**
     * vraci nazev postavy
     * @return nazev postavy
     */
    public String getJmeno() {
        return this.jmeno;
    }
    /** vraci proslov postavy
     *
     * @return proslov postavy
     */
    public String getProslov() {
        return this.proslov;
    }

    public String getTextKteryReknePoDruhe() {
        return textKteryReknePoDruhe;
    }

    /** konstruktor postavy
     *
     * @param jmeno    jmeno postavy
     * @param proslov  prvni,co postava rekne
     * @param proslov2 proslov po ziskani veci
     */

    public Postava(String jmeno, String proslov, String proslov2, String proslov3, String proslov4) {
        this.jmeno = jmeno;
        this.proslov = proslov;
        this.textKteryRekneKdyzDostaneSvouVec = proslov2;
        this.textKteryReknePoDruhe = proslov3;
        this.textKteryRekneKdyzDostaneSvouVecPoDruhe = proslov4;


}
}
