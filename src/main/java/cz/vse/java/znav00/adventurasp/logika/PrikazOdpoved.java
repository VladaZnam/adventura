package cz.vse.java.znav00.adventurasp.logika;
/**
 * Trida PrikazOdpoved implementuje pro hru prikaz odpoved
 * Trida ja soucasti jednoduche textove hry
 *
 * @author Vlada Znamenshchikova
 * @version cerven 2020
 */

public class PrikazOdpoved implements IPrikaz {
    HerniPlan herniPlan;
    private static final String NAZEV = "odpoved";

    /**
     * Konstruktor tridy
     * @param plan
     */

    public PrikazOdpoved( HerniPlan plan) {

        this.herniPlan = plan;

    }


    @Override
    public String provedPrikaz(String... parametry) {

        if (parametry.length == 0) {
            // pokud chybí druhé slovo, tak ....
            return "Co mam odpovedet? Musite zadat odpoved.";
        }
        else {
            if (herniPlan.getAktualniProstor().getNazev().equals("knihovna") ) {//jestlize v spravnem prostoru
                if (parametry.length == 1 && parametry[0].equals("ticho")) {//spravna odpoved
                    Vec vecKterouDavaPostava = herniPlan.getAktualniProstor().getPostava().getVecKterouPostavaDava();

                    if (vecKterouDavaPostava != null) {
                        herniPlan.getBatoh().vlozVeciDoBatohu(vecKterouDavaPostava);

                        { return "Vyhral jsi." + herniPlan.getAktualniProstor().getPostava().getTextKteryRekneKdyDostaneSvouVec() ;

                }}}

                return "Zkus jeste jednou";
            }
            return "Tento prikaz tady neplati";
        }

    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
