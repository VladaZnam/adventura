package cz.vse.java.znav00.adventurasp.logika;

import javafx.scene.image.Image;

import javax.swing.*;
import java.util.*;

/**
 * Trida Batoh popisuje batoh,do ktereho je mozna ukladat veci.
 * Batoh ma limit veci(7 kusu)
 * Do batohu je mozna ukladat a vybirat veci
 * Batoh je tvoren mapou
 *
 * @author Vlada Znamenshchikova
 * @version cerven 2020
 *
 */

public class Batoh {
    private Map<String, Vec> mapaSVecmi;
    private final int limit = 3;
    HerniPlan plan;
    private Set<String> mapaSVecmiKtereBudeVypisovatDoFormulare;
    private Set<Image> mapaObrazkuBatoh;

    /**
     * konstruktor batohu
     *
     */

    public Batoh(HerniPlan plan) {
        mapaSVecmi = new HashMap<>();
        this.plan = plan;
        mapaSVecmiKtereBudeVypisovatDoFormulare = new HashSet<>();
        mapaObrazkuBatoh = new HashSet<>();
    }

    public Set<String> getMapaSVecmiKtereBudeVypisovatDoFormulare(){
        return mapaSVecmiKtereBudeVypisovatDoFormulare;
    }

    public Batoh() {

    }

    /**
     * Metoda vklada veci do batohu
     */
    public boolean vlozVeciDoBatohu(Vec vec) {
        if (mapaSVecmi.size() < limit) {
            mapaSVecmi.put(vec.getNazev(), vec);
            mapaSVecmiKtereBudeVypisovatDoFormulare.add(vec.getNazev());
            mapaObrazkuBatoh.add(vec.getObrazek());
            plan.notifyObservers();
            return true;
        }
        return false;
    }

    public boolean jePlniBatoh( ) {
        if (mapaSVecmi.size() >= limit ){
            return true;

        } return false;
    }

    /**
     * Metoda vklada veci z batohu
     */
    public Vec dejVecZBatohu(String retezec) {
        //metoda pro vyndani jedne veci z batohu
        if (mapaSVecmi.containsKey(retezec)) {
            Vec temp = mapaSVecmi.get(retezec);
            //ulozim si vec z batohu, kterou cheme vyndat
            mapaSVecmi.remove(retezec);
            mapaSVecmiKtereBudeVypisovatDoFormulare.remove(retezec);
            mapaObrazkuBatoh.remove(retezec);
            plan.notifyObservers();
            return temp;
        } else {
            return null;
        }
    }

    /**
     * Zkontorlue, zda je vec v batohu
     */
    public boolean jeVecVBatohu(String retezec) {
        return mapaSVecmi.containsKey(retezec);
    }

    /**
     * Zjistit ci je v batohu
     * @return seznam veci v batohu
     */
    public String vratNazvyVeciVBatohu() {
        //vypise seznam veci, ktere jsou v batohu
        String seznam = "";
        for (String nazevVeci : mapaSVecmi.keySet()) {
            seznam += nazevVeci + " ";
        }
        if (seznam == "") seznam = "Batoh je prazdny.";
        return seznam;
    }


    public Image[] getObrazky(){
        Image[] obrazky = new Image[4];
        int i = 0;
        for (Map.Entry<String,Vec> vec : mapaSVecmi.entrySet()){
            obrazky[i] = vec.getValue().getObrazek();
            i++;

        } return obrazky;
    }

}

