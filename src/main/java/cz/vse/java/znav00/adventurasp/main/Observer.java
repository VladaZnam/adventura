package cz.vse.java.znav00.adventurasp.main;

public interface Observer {
     /*
    metoda kterou vola predmet pozorovani ( subject ) pri zmene

     */

    void update();
}
