package cz.vse.java.znav00.adventurasp.logika;

/**
 * Trida PrikazMluv implementuje pro hru prikaz mluv.
 * Trida ja soucasti jednoduche textove hry.
 *
 * @author Vlada Znamenshchikova
 * @version cerven 2020
 */

public class PrikazMluv implements IPrikaz {
    private static final String NAZEV = "mluv";
    final private HerniPlan herniPlan;

    public PrikazMluv(HerniPlan herniPlan) {
        this.herniPlan = herniPlan;
    }

    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            //kdyby uzivatel nezadal jmeno postavy, se kterou ma mluvit, tak se vypise jmeno postavy v mistnosti
            if (herniPlan.getAktualniProstor().getPostava() != null) {
                return "Nevim s kym mam mluvit.V mistnisti je:" + herniPlan.getAktualniProstor().getPostava().getJmeno() +
                        ". Pokud chces mluvit s postavou, napis 'mluv " + herniPlan.getAktualniProstor().getPostava().getJmeno() + ".";


            } else {
                return "Neni tu s kym mluvit.";
            }
        } else {
            String jmenoPostavy = parametry[0];



                if (herniPlan.getBatoh().jeVecVBatohu("klic2")) {
                     Vec vecKterouDavaPostava2 = herniPlan.getAktualniProstor().getPostava().getVecKterouPostavaDava2();

                    if (vecKterouDavaPostava2 != null) {
                    herniPlan.getBatoh().vlozVeciDoBatohu(vecKterouDavaPostava2);
                    {
                        return herniPlan.getAktualniProstor().getPostava().getTextKteryReknePoDruhe();
                    }
                } return herniPlan.getAktualniProstor().getPostava().getTextKteryReknePoDruhe();
            } else {

            if (!Hra.equals(herniPlan.getAktualniProstor().getPostava().getJmeno(), jmenoPostavy)) {
                {
                    //kdyby uzivatel zdal jmeno postavy, ktera neni v mistonosti nebo nejaky jiny nemsysl
                    return "Tato postava v teto mistnosti neni."; }}
            //pokud je vse ok, tak mi to vrati proslov postavy
            else return herniPlan.getAktualniProstor().getPostava().getProslov(); }}}






        /**
         * Metoda vraci nazev prikazu (slovo,ktere poumuvziva hrac pro jeho vyvolani)
         *
         * @return nazev prikazu
         */

        @Override
        public String getNazev () {
            return NAZEV;
        }

    }






