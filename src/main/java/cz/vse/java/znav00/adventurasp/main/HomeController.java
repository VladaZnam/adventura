package cz.vse.java.znav00.adventurasp.main;

import cz.vse.java.znav00.adventurasp.logika.Hra;
import cz.vse.java.znav00.adventurasp.logika.IHra;
import cz.vse.java.znav00.adventurasp.logika.PrikazJdi;
import cz.vse.java.znav00.adventurasp.logika.Prostor;
import javafx.application.Platform;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import javax.swing.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeController implements Observer {
    @FXML private ImageView hrac;
    @FXML private ListView panelBatoh;
    @FXML private ListView panelVychodu;
    @FXML private TextArea vystup;
    @FXML private TextField vstup;
    @FXML private Button odesli;

    @FXML private ComboBox prikazy;
    @FXML private HBox pozady;
    private IHra hra ;
    private Map<String, Point2D> souradnice;
    @FXML private ImageView obr1;
   @FXML private ImageView obr2;
    @FXML private ImageView obr3;
    private Start parentStartClass;
    public void setParentStartClass(Start parentStartClass){this.parentStartClass = parentStartClass;}

    public Start getParentStartClass(){
        return parentStartClass;
    }



    @FXML
    private void initialize(){
       hra = new Hra();
       vystup.setWrapText(true);
        vystup.appendText(hra.vratUvitani()+"\n\n");
      panelVychodu.getItems().addAll(hra.getHerniPlan().getAktualniProstor().getVychody());

        ObservableList<String> listPrikazu = FXCollections.observableArrayList();

        listPrikazu.add("dej");
        listPrikazu.add("seber");
        listPrikazu.add("jdi");
        listPrikazu.add("mluv");
        listPrikazu.add("odpoved");
        listPrikazu.add("zaklinadlo");
        listPrikazu.add("napoveda");
        listPrikazu.add("batoh");
        listPrikazu.add("konec");
        prikazy.setItems((ObservableList) listPrikazu);
        prikazy.setValue("jdi");

       // vystup.setText(hra.vratUvitani() + "\n\n");
        //vystup.setEditable(false);

        //vystup.setEditable(false);
       //panelVychodu.getItems().addAll(hra.getHerniPlan().getAktualniProstor().getVychody());
        //panelBatoh.getItems().addAll(hra.getHerniPlan().getBatoh().vratNazvyVeciVBatohu());

        hra.getHerniPlan().register(this);

        souradnice = createSouradnice();



        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                vstup.requestFocus();
            }
        });


    }


    @FXML private void zppacujVstup(ActionEvent actionEvent) {
        String prikaz = prikazy.getValue()+" "+ vstup.getText();
        zpracujPrikaz(prikaz);
        vstup.clear();


        //aby nove okinko
        /*if (prikaz.equals("batoh")){
            Stage stage = new Stage();
            stage.setScene(new Scene(new GridPane()));
            stage.show();
        }*/


    }



    private void zpracujPrikaz (String prikaz){
        vystup.appendText("Příkaz: "+prikaz+"\n");
        vystup.appendText(hra.zpracujPrikaz(prikaz)+"\n\n");

        if(hra.konecHry()){
            vystup.appendText(hra.vratEpilog());
            vstup.setDisable(true);
            odesli.setDisable(true);
            panelVychodu.setDisable(true);
        }

    }

    @Override
    public void update() {

        Prostor aktualniProstor = hra.getHerniPlan().getAktualniProstor();
        panelVychodu.getItems().clear();
        panelVychodu.getItems().addAll(hra.getHerniPlan().getAktualniProstor().getVychody());
       // panelBatoh.getItems().clear();
        //panelBatoh.getItems().addAll(hra.getHerniPlan().getBatoh().vratNazvyVeciVBatohu());

        hrac.setLayoutX(souradnice.get(aktualniProstor.getNazev()).getX());
        hrac.setLayoutY(souradnice.get(aktualniProstor.getNazev()).getY());


        List<ImageView> imageList = new ArrayList<>();
        imageList.add(obr1);
        imageList.add(obr2);
        imageList.add(obr3);

        int i=0;
        for (ImageView img : imageList){
            img.setImage(hra.getHerniPlan().getBatoh().getObrazky()[i]);
            i++;
        }


    }

    private Map<String, Point2D> createSouradnice(){
        Map<String, Point2D> souradnice = new HashMap<>();
        souradnice.put("palouk",new Point2D(-18,151));
        souradnice.put("pokoj_s_krbem",new Point2D(67,134));
        souradnice.put("knihovna",new Point2D(28,73));
        souradnice.put("pokoj_vladce",new Point2D(69,11));
        return souradnice;

    }

    public void vybranVychod(MouseEvent mouseEvent) {
        Prostor prostor = (Prostor) panelVychodu.getSelectionModel().getSelectedItem();
        if (prostor!=null) {
            zpracujPrikaz(PrikazJdi.NAZEV+" "+ prostor.getNazev());
        }


    }









    public void knutiNapoveda(ActionEvent actionEvent) {
        Stage stage1 = new Stage();
        stage1.setTitle("Nápověda");
        WebView webView = new WebView();
        webView.getEngine().load(Start.class.getResource("/Napoveda.html").toExternalForm());
        stage1.setScene(new Scene(webView, 500, 500 ));
        stage1.show();
    }

    public void kliknutiKonec(ActionEvent actionEvent) {
        System.exit(0);
    }

    public void newGame(ActionEvent actionEvent) throws Exception {
        vystup.clear();
        hra = new Hra();
        vystup.appendText(hra.vratUvitani()+"\n\n");
        panelVychodu.getItems().addAll(hra.getHerniPlan().getAktualniProstor().getVychody());
        hra.getHerniPlan().register(this);
        hra.getHerniPlan().notifyObservers();
        //parentStartClass.getMyStage().close();
        //Stage temp = new Stage();
        //parentStartClass.setMyStage(temp);
        //parentStartClass.start(temp);

    }

    public void oProgramu(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Adventura y grafickým rozhráním");
        alert.setHeaderText("JavaFX adventura");
        alert.setContentText("verze ZS 2020");

        alert.showAndWait();
    }
}
