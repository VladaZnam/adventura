package cz.vse.java.znav00.adventurasp.logika;

import javafx.scene.image.Image;

/**
 * Trida popisuje veci ktere se nachazi v prostorech nebo u jinych postat hry
 * Vec muze byt prenositelna, pak muzeme ji vlozit do batohu nebo neprositelna
 *
 * @autor Vlada Znamenshchikova
 * @version cerven 2020
 */

public class Vec {

    private final String nazev;
    private final String popis;
    private final boolean prenositelna;
    private final Image obrazek;

    public Vec(String nazev, String popis, boolean prenositelna, Image obrazek) {
        this.nazev = nazev;
        this.popis = popis;
        this.prenositelna = prenositelna;

        this.obrazek = obrazek;
    }

    /**
     * Vraci nazev veci
     *
     * @return
     */

    public String getNazev() {
        return this.nazev;
    }

    /** Nastvi prenositelnost veci
     *
     * @return prenositelnost veci true - vec prenositelna
     *                             false - neprenositelna
     */


    public String getPopis() {
        return popis;
    }

    public boolean isPrenositelna() {
        return prenositelna;
    }

    public Image getObrazek() { return obrazek;
    }
}
