package cz.vse.java.znav00.adventurasp.logika;

/**
 * Trida PrikazBatoh implementuje pro hru prikaz batoh.
 * Trida ja soucasti jednoduche textove hry
 *
 * @author Vlada Znamenshchikova
 * @version cerven 2020
 */


public class PrikazBatoh implements IPrikaz {
    HerniPlan herniPlan;
    private static final String NAZEV = "batoh";

    /**
     * Konstruktor tridy
     * @param plan
     */

    public PrikazBatoh( HerniPlan plan) {
        this.herniPlan = plan;
    }

    /**
     *
     * @param parametry počet parametrů závisí na konkrétním příkazu.
     * @return
     */

    @Override
    public String provedPrikaz(String... parametry) {
        return herniPlan.getBatoh().vratNazvyVeciVBatohu();

    }

    @Override
    public String getNazev(){
        return NAZEV;}
    }


