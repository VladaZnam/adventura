package cz.vse.java.znav00.adventurasp.logika;

/**
 * Třída PrikazJdi implementuje pro hru příkaz jdi.
 * Tato třída je součástí jednoduché textové hry.
 *
 * @author Vlada Znamenshchikova
 * @version cerven 2020
 */
public class PrikazJdi implements IPrikaz {
    public static final String NAZEV = "jdi";
    private HerniPlan plan;

    /**
     * Konstruktor třídy
     *
     * @param plan herní plán, ve kterém se bude ve hře "chodit"
     */
    public PrikazJdi(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Provádí příkaz "jdi". Zkouší se vyjít do zadaného prostoru. Pokud prostor
     * existuje, vstoupí se do nového prostoru. Pokud zadaný sousední prostor
     * (východ) není, vypíše se chybové hlášení.
     *
     * @param parametry - jako  parametr obsahuje jméno prostoru (východu),
     *                  do kterého se má jít.
     * @return zpráva, kterou vypíše hra hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední prostor), tak ....
            return "Kam mám jít? Musíš zadat jméno východu";
        }

        String smer = parametry[0];
        boolean pruhodPovolen = false; // boolean o tom zda mohu jit do mistnosti kterou uzivatel zadal
        // zkoušíme přejít do sousedního prostoru
        Prostor sousedniProstor = plan.getAktualniProstor().vratSousedniProstor(smer);

        if (sousedniProstor == null) {
            return "Tam se odsud jít nedá!";
        }
        switch (sousedniProstor.getNazev()) {
            case "knihovna":
                //zkontroluje zda je pruhod mezi mistnostmi pokoj s krbem a knihovna
                if (plan.getBatoh().jeVecVBatohu("klic")) {
                    pruhodPovolen = true;
                }
                break;
            default:
                pruhodPovolen = true;
                break;

            case "pokoj_vladce":
                //zkontroluje zda je pruhod mezi mistnostmi knihovna a pokoj vladce
                if (plan.getBatoh().jeVecVBatohu("klic2")) {
                    pruhodPovolen = true;
                }
                break;

        }
        if (pruhodPovolen == true) {
            //pro pripad, kdybych chtel jit jinam v case, kam jeste nemuzu
            plan.setAktualniProstor(sousedniProstor);
            return sousedniProstor.dlouhyPopis();
        } else return "Pruchod neni povolen";
    }


    /**
     * Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     * @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
