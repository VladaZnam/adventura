/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package cz.vse.java.znav00.adventurasp.main;


import cz.vse.java.znav00.adventurasp.logika.Hra;
import cz.vse.java.znav00.adventurasp.logika.IHra;
import cz.vse.java.znav00.adventurasp.uiText.TextoveRozhrani;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/*******************************************************************************
 * Třída  Start je hlavní třídou projektu,
 * který představuje jednoduchou textovou adventuru určenou k dalším úpravám a rozšiřování
 *
 * @author Vlada Znamenshchikova
 * @version cerven 2020
 */

public class Start extends Application
{
    private Stage myStage;

    /***************************************************************************
     * Metoda, prostřednictvím níž se spouští celá aplikace.
     *
     * @param args Parametry příkazového řádku
     */
    public static void main(String[] args)
    {
        if ((args.length==1) && args[0].equals("-text")){
        IHra hra = new Hra();
        TextoveRozhrani ui = new TextoveRozhrani(hra);
        ui.hraj();
        } else {
        launch();
        }

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/home.fxml"));
        GridPane root = loader.load();
        HomeController controller = loader.getController();
        primaryStage.setScene(new Scene(root));
        primaryStage.setTitle("Adventura");
        primaryStage.show();

    }
    
    public Stage getMyStage() {
        return myStage;
    }

    public void setMyStage(Stage myStage){
        this.myStage = myStage;
    }
}
