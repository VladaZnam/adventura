package cz.vse.java.znav00.adventurasp.logika;
/**
 * Trida PrikazSeber implementuje pro hru prikaz seber
 * Trida ja soucasti jednoduche textove hry
 *
 * @author Vlada Znamenshchikova
 * @version cerven 2020
 */

public class PrikazSeber implements IPrikaz {

    private static final String SEBER = "seber";
    private final HerniPlan herniPlan;

    public PrikazSeber(HerniPlan herniPlan) {
        this.herniPlan = herniPlan;
    }

    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Nevim co mam sebrat.Podivej se na popis mistnosty, tam jsou veci";
        }

        String nazevVeci = parametry[0];
        Prostor aktualniProstor = herniPlan.getAktualniProstor();

        if (!aktualniProstor.obsahujeVec(nazevVeci)) {
            return "Vec s nazvem " + nazevVeci + " v prostoru neexistuje";

        } else {
            Vec vec = aktualniProstor.vreatVec(nazevVeci);
            if (vec.isPrenositelna()) {
                if (!herniPlan.getBatoh().jePlniBatoh()) {
                    Vec temp = herniPlan.getAktualniProstor().odstranVec(nazevVeci);
                    herniPlan.getBatoh().vlozVeciDoBatohu(temp);
                    //vlozeni do batohu
                    return "Sebral jsi vec " + nazevVeci;
                } else { return  "Batoh je plni";

                }} else {
                return "Tuto vec neni mozne prinest";
            }
        }


    }

    @Override
    public String getNazev() {
        return SEBER;
    }
}
