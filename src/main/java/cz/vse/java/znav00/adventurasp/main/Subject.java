package cz.vse.java.znav00.adventurasp.main;

public interface Subject {
     /*
    registrace przorovatele
     pozorovatelum se posila zmenu
     @param observer
     */

    void register(Observer observer);
    /*
       odebrani przorovatele ze seznamu
       @param observer
        */
    void unregister(Observer observer);
    /*
    upozorni vsechy registroovane pozorovatele na zmenu
    neni vhodna jako verejna metoda do rozhrani
    bez param protoze pro vsechny
     */

    void notifyObservers();

}
